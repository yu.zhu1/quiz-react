import React from 'react';
import Guess from "./Guess";
import './PlayingPanel.less'

class PlayingPanel extends React.Component{

  constructor(props, context) {
    super(props, context);
    this.state = {
      rightLetters: '',
      guessLetters: ''
    };
    this.getGuess = this.getGuess.bind(this);
    this.getGuessResults = this.getGuessResults.bind(this);
  }

  render() {
    return (
      <div className='playPanel'>
        <div className='yourResult'>
          <h1>Your Result</h1>
          <ul className='newGame'>
            <div>{this.state.guessLetters.charAt(0)}</div>
            <div>{this.state.guessLetters.charAt(1)}</div>
            <div>{this.state.guessLetters.charAt(2)}</div>
            <div>{this.state.guessLetters.charAt(3)}</div>
          </ul>
          {this.state.judgement}
        </div>
        <Guess getGuessLetters = {this.getGuess} getGuessResults={this.getGuessResults} clearGuessLetters = {this.props.clearStatus}/>
      </div>
    )
  }

  getGuess(guessLetters) {
    this.setState({
      guessLetters: guessLetters});
  }

  getGuessResults() {
    if (this.state.guessLetters !== '') {
      let thisStatus = this.props.compare === this.state.guessLetters;
      this.setState({judgement: thisStatus? <h2 style = {{color: 'red'}}>SUCCESS</h2> : <h2 style={{color:'green'}}>FAILED</h2>});
    }
  }





}
export default PlayingPanel;