import React from 'react';
import ControlPanel from "./ControlPanel";
import PlayingPanel from "./PlayingPanel";
import './App.less';

class App extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      rightLetters: '',
      clearStatus: false
    };
    this.getAnswer = this.getAnswer.bind(this);
    this.getClearStatus = this.getClearStatus.bind(this);
  }

  render() {
    return (
      <div className='game'>
        <PlayingPanel  compare = {this.state.rightLetters} clearStatus = {this.state.clearStatus}/>
        <ControlPanel  answer = {this.getAnswer}  clear = {this.getClearStatus}/>
      </div>
    );
  }

  getAnswer(letters) {
    this.setState({
      rightLetters: letters
    });
  }

  getClearStatus(clearStatus) {
    this.setState({clearStatus:clearStatus})
  }

}

export default App;