import React from 'react';
import Answer from "./Answer";
import './ControlPanel.less';


class ControlPanel extends React.Component{

  constructor(props, context) {
    super(props, context);
    this.state = {
      letters:'',
      showFlag: false
    };
    this.createAnswer = this.createAnswer.bind(this);
    this.getAnswer = this.getAnswer.bind(this);
  }

  render() {
    return (
      <div className='controlPanel'>
        <div className='showAnswer'>
          <h1>New Card</h1>
          <button id='newGame' onClick={this.createAnswer} >New Card</button>
          <Answer show = {this.state.showFlag} letters = {this.state.letters}/>
        </div>
        <div className='showResult'>
          <button id='showResult' onClick={this.getAnswer}>Show Result</button>
        </div>
      </div>
    );
  }

  createAnswer() {
    let showStatus = !this.state.showFlag;
    let letters = ControlPanel.generateLetters();
    this.setState(
      {letters: letters,showFlag: showStatus});
    this.props.answer(letters);
    this.props.clear(true);
    setTimeout(()=>this.setState({showFlag: false}),3000)
  }

  static generateLetters() {
    let letters = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    const charactersLength = characters.length;
    for (let i = 0; i < 4; i++) {
      letters += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return letters;
  }

  getAnswer() {
    let showStatus = !this.state.showFlag;
    this.setState(
      {showFlag: showStatus}
    )
  }


}

export default ControlPanel;