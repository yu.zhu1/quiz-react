import React from 'react';
import './Answer.less';
class Answer extends React.Component{


  constructor(props, context) {
    super(props, context);

  }

  render() {
    if (this.props.show) {
      return (
        <div className='answer'>
          <ul className='showAnswer'>
            <div>{this.props.letters.charAt(0)}</div>
            <div>{this.props.letters.charAt(1)}</div>
            <div>{this.props.letters.charAt(2)}</div>
            <div>{this.props.letters.charAt(3)}</div>
          </ul>
        </div>
      );
    } else {
      return null;
    }
  }
}

export default Answer;