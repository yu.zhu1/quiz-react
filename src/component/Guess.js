import React from 'react';
import './Guess.less';

class Guess extends React.Component{

  constructor(props, context) {
    super(props, context);
    this.state = {
      inputLetters:null
    };
    this.inputLetters = this.inputLetters.bind(this);
    this.guessResult = this.guessResult.bind(this);
  }

  render() {
    return (
      <div className='guess'>
        <h1>Guess Card</h1>
        <input className='inputLetters' maxLength="4" onChange={this.inputLetters}/><br/>
        <button id = 'guess' type="submit" onClick={this.guessResult}>Guess</button>
      </div>
      )
  }


  inputLetters(event) {
    this.props.getGuessLetters(event.target.value.toUpperCase());
    this.setState({
      inputLetters: event.target.value.toUpperCase()
    });
  }


  guessResult() {
    this.props.getGuessResults();
  }
}

export default Guess;